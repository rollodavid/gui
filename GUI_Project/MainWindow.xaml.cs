﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Diagnostics;
using System.IO;

namespace GUI_Project
{
    /// <summary>
    /// Interakční logika pro MainWindow.xaml
    /// </summary>

    public partial class MainWindow : Window
    {
        #region Variables
        readonly Random nc = new Random();
        Window winLogin;
        Window winRegistr;
        Window emailWPF;
        #endregion

        public MainWindow()
        {
            InitializeComponent();
        }

        #region Uložení/Načtení do/ze souboru

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //Načtení účtů ze souboru
            SaveAndLoad.LoadAccounts();
        }
        
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            //Uložení účtů do souboru
            SaveAndLoad.SaveAccounts();
        }

        #endregion

        #region Menu

        private void ShowAccounts_Click(object sender, RoutedEventArgs e)
        {
            //Zobrazení vytvořených účtů
            if (File.Exists(SaveAndLoad.Path)) Process.Start(SaveAndLoad.Path);
        }

        private void Website_Click(object sender, RoutedEventArgs e)
        {
            //Zobrazení webové stránky
            Process.Start("https://stag.vajsbejn.cz/");
        }

        private void Email_Click(object sender, RoutedEventArgs e)
        {
            //Pokud bude již nějaké okno emailu otevřené, zobrazí se to
            if (emailWPF != null && emailWPF.Activate()) emailWPF.Show();
            //Jinak vytvoří nové okno
            else
            {
                emailWPF = new EmailWPF();
                emailWPF.Show();
            }
        }

        private void Logout_Click(object sender, RoutedEventArgs e)
        {
            Accounts.LogAccount = null;
            logAccount.Content = "Uknown";
            btnLogin.Visibility = Visibility.Visible;
            btnRegister.Visibility = Visibility.Visible;
        }

        private void Minimize_App(object sender, RoutedEventArgs e)
        {
            WindowState = WindowState.Minimized;
        }

        private void Close_App(object sender, RoutedEventArgs e)
        {
            MessageBoxResult msr = MessageBox.Show($"Opravdu chceš odejít?", "Konec", MessageBoxButton.YesNo);
            if (msr == MessageBoxResult.Yes) App.Current.Shutdown();
        }

        private void Grid_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed) DragMove();
        }

        #endregion

        #region Login/Register

        private void Login_Click(object sender, RoutedEventArgs e)
        {
            line1.Background = new SolidColorBrush(Color.FromRgb((byte)nc.Next(0, 256), (byte)nc.Next(0, 256), (byte)nc.Next(0, 256)));
            line2.Background = new SolidColorBrush(Color.FromRgb((byte)nc.Next(0, 256), (byte)nc.Next(0, 256), (byte)nc.Next(0, 256)));
            line3.Background = new SolidColorBrush(Color.FromRgb((byte)nc.Next(0, 256), (byte)nc.Next(0, 256), (byte)nc.Next(0, 256)));

            winLogin = new LoginWPF();
            winLogin.ShowDialog();

            if (Accounts.LogAccount != null)
            {
                logAccount.Content = Accounts.LogAccount[0];
                btnLogin.Visibility = Visibility.Hidden;
                btnRegister.Visibility = Visibility.Hidden;
            }
            else logAccount.Content = "Uknown";
        }

        private void Registration_Click(object sender, RoutedEventArgs e)
        {
            line1.Background = new SolidColorBrush(Color.FromRgb((byte)nc.Next(0, 256), (byte)nc.Next(0, 256), (byte)nc.Next(0, 256)));
            line2.Background = new SolidColorBrush(Color.FromRgb((byte)nc.Next(0, 256), (byte)nc.Next(0, 256), (byte)nc.Next(0, 256)));
            line3.Background = new SolidColorBrush(Color.FromRgb((byte)nc.Next(0, 256), (byte)nc.Next(0, 256), (byte)nc.Next(0, 256)));

            winRegistr = new RegistrWPF();
            winRegistr.ShowDialog();
        }

        #endregion

        private void API_Click(object sender, RoutedEventArgs e)
        {
            Dictionary<string, object> weatherControls = new Dictionary<string, object>()
            {
                {"icon", picIcon},
                {"city", weatherCity},
                {"decription", weaterDecription},
                {"temp", weaterTemp},
                {"windSpeed", weaterWindSpeed},
            };

            API.StartAPI(cityName.Text, weatherControls);
        }
    }
}
