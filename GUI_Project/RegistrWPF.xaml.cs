﻿using System;
using System.Windows;
using System.Windows.Input;
using System.Net.Mail;

namespace GUI_Project
{
    /// <summary>
    /// Interakční logika pro RegistrWPF.xaml
    /// </summary>
    public partial class RegistrWPF : Window
    {
        #region Variables
        readonly int minDataLength = 1; //Minimální délka údajů (jméno, heslo, email)
        #endregion

        public RegistrWPF()
        {
            InitializeComponent();
        }

        // Registrace
        private void Reg_Click(object sender, RoutedEventArgs e)
        {
            //Údaje musí mít délku minimálně minDataLength
            if (regName.Text.Length < minDataLength || regEmail.Text.Length < minDataLength || regPass.Password.Length < minDataLength)
            {
                MessageBox.Show("The length of each data must be at least " + minDataLength, "Account creation", MessageBoxButton.OK, MessageBoxImage.Asterisk);
                return;
            }

            //Formát emailu
            try
            {
                _ = new MailAddress(regEmail.Text); //Toto nekontroluje přípony .cz a .com
                //Pokud email neobsahuje "seznam.cz" nebo "gmail.com", tak má špatný formát, jelikož pouze z těchto adres mohu odesílat emaily
                if (!regEmail.Text.Contains("seznam.cz") && !regEmail.Text.Contains("gmail.com"))
                {
                    MessageBox.Show("\nEmail address has wrong format", "Account creation", MessageBoxButton.OK, MessageBoxImage.Asterisk);
                    return;
                }
            }
            catch (Exception)
            {
                MessageBox.Show("\nEmail address has wrong format", "Account creation", MessageBoxButton.OK, MessageBoxImage.Asterisk);
                return;
            }

            //Zkouška přidání účtu
            if (Accounts.CreateNewAccount(regName.Text, regPass.Password, regEmail.Text))
            {
                MessageBox.Show("Account was created", "Account creation", MessageBoxButton.OK, MessageBoxImage.Information);
                Close();
            }
            else MessageBox.Show("This account is already created", "Account creation", MessageBoxButton.OK, MessageBoxImage.Warning);
        }

        // Pohyb oknem
        private void Reg_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed) DragMove();
        }

        // Zavírání okna
        private void Reg_Close_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
