﻿using System;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Input;
using System.Net;
using System.Net.Mail;
using System.Diagnostics;

namespace GUI_Project
{
    /// <summary>
    /// Interakční logika pro EmailWPF.xaml
    /// </summary>

    public partial class EmailWPF : Window
    {
        #region Variables
        readonly SmtpClient smtp = new SmtpClient();
        readonly MailMessage mail = new MailMessage();
        #endregion

        public EmailWPF()
        {
            InitializeComponent();
        }

        private void SendMail_Click(object sender, RoutedEventArgs e)
        {
            if (tbFrom.Text.Contains("seznam"))
            {
                smtp.Host = "smtp.seznam.cz";
                smtp.Port = 587;
                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtp.EnableSsl = false;
                smtp.UseDefaultCredentials = false;
                //Jmého si beze z emailové adresy odesílatele z části před @
                smtp.Credentials = new NetworkCredential(tbFrom.Text.Substring(0, tbFrom.Text.IndexOf('@')), tbPassword.Password);
            }
            else if (tbFrom.Text.Contains("gmail"))
            {
                smtp.Host = "smtp.gmail.com";
                smtp.Port = 587;
                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtp.EnableSsl = true;
                smtp.UseDefaultCredentials = false;
                smtp.Credentials = new NetworkCredential(tbFrom.Text, tbPassword.Password);
            }
            else
            {
                MessageBox.Show("Email lze odeslat pouze prostřednictvím Seznamu nebo Gmailu!", "Email", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }

            try
            {
                mail.From = new MailAddress(tbFrom.Text);
                mail.Subject = tbSubject.Text;
                mail.Body = new TextRange(rtbBody.Document.ContentStart, rtbBody.Document.ContentEnd).Text;
                mail.To.Clear();
                mail.To.Add(tbTo.Text);

                Debug.WriteLine($@"
From:    {mail.From.Address}
To:      {mail.To}
Subject: {mail.Subject}
Body:    {mail.Body}
");

                smtp.Send(mail);
                MessageBox.Show("The email has been sent successfully", "Email", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            catch (SmtpException smtpEx)
            {
                MessageBox.Show(smtpEx.Message + "\nPokud zasíláte z účtu od Seznamu:\n 1) Zadali jste špatné údaje\n 2) Máte zapnuté dvoufázové ověřování\n - s tím zaslat email nelze", "Email se nepodařilo odeslat!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message, "Email se nepodařilo odeslat!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        // Pohyb oknem
        private void Menu_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed) DragMove();
        }

        // Zavírání okna
        private void Close_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        //Načtení emailu, pokud je uživatel přihlášen
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (Accounts.LogAccount != null) tbFrom.Text = Accounts.LogAccount[2];
        }
    }
}
