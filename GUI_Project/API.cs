﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using Newtonsoft.Json;
using System.Windows.Controls;
using System.Windows.Media.Imaging;

namespace GUI_Project
{
    public class API
    {
        #region Variables
        static string city;
        static readonly string unitsType = "metric"; //Získám °C místo Kelvinů
        static readonly string appid = "4e5deadbfffca4bca321598df873c692";
        static string url;
        static string json;
        static WeatherInfo.Root Info;
        #endregion

        public static void StartAPI(string cityName, Dictionary<string, object> weatherConstrols)
        {
            using (WebClient web = new WebClient())
            {
                city = cityName;

                url = $"https://api.openweathermap.org/data/2.5/weather?q={city}&units={unitsType}&appid={appid}";

                try
                {
                    //JSON data ve Stringu
                    json = Encoding.UTF8.GetString(web.DownloadData(url));
                    //Překonvertování stringových dat na Třídy
                    Info = JsonConvert.DeserializeObject<WeatherInfo.Root>(json);

                    //Nastavení vizualizace, když je město známé
                    ((Image)weatherConstrols["icon"]).Source = new BitmapImage(new Uri($"https://openweathermap.org/img/w/{Info.Weather[0].Icon}.png"));

                    ((TextBlock)weatherConstrols["city"]).Text = $"{Info.Name} ({Info.Sys.Country})";
                    ((TextBlock)weatherConstrols["decription"]).Text = char.ToUpper(Info.Weather[0].Description[0]) + Info.Weather[0].Description.Substring(1);
                    ((TextBlock)weatherConstrols["temp"]).Text = $"{Info.Main.Temp} °C";
                    ((TextBlock)weatherConstrols["windSpeed"]).Text = $"{Info.Wind.Speed} m/s {Info.Wind.DegSide} ({Info.Wind.Deg}°)";
                }
                catch (Exception)
                {
                    //Nastavení vizualizace, když je město neznámé
                    ((Image)weatherConstrols["icon"]).Source = new BitmapImage(new Uri("Images/temperatures.png", UriKind.Relative));

                    ((TextBlock)weatherConstrols["city"]).Text = city + "?";
                    ((TextBlock)weatherConstrols["decription"]).Text = "...";
                    ((TextBlock)weatherConstrols["temp"]).Text = "...";
                    ((TextBlock)weatherConstrols["windSpeed"]).Text = "...";
                }
            }
        }
    }

    public class WeatherInfo
    {
        public class Root
        {
            public List<Weather> Weather { get; set; }
            public Main Main { get; set; }
            public Wind Wind { get; set; }
            public Sys Sys { get; set; }

            public string Name { get; set; }
        }

        public class Weather
        {
            public string Description { get; set; } //Popis
            public string Icon { get; set; } //Icona
        }

        public class Main
        {
            public double Temp { get; set; } //Teplota (Kelviny)
        }

        public class Wind
        {
            public double Speed { get; set; } //Rychlost větru (m/s)
            public double Deg { get; set; } //Směr větru (°)
            public string DegSide
            {
                get
                {
                    if (Deg > 337.5 | Deg <= 22.5) return "East";
                    else if (Deg > 22.5 & Deg <= 67.5) return "Northeast";
                    else if (Deg > 67.5 & Deg <= 112.5) return "North";
                    else if (Deg > 112.5 & Deg <= 157.5) return "Northwest";
                    else if (Deg > 157.5 & Deg <= 202.5) return "West";
                    else if (Deg > 202.5 & Deg <= 247.5) return "Southwest";
                    else if (Deg > 247.5 & Deg <= 292.5) return "South";
                    else return "Southeast";
                }
            }
        }

        public class Sys
        {
            public string Country { get; set; }
        }
    }
}
