﻿using System;
using System.Collections.Generic;

namespace GUI_Project
{
    class Accounts
    {
        //Hlavní uložiště účtů
        readonly static Dictionary<string, string[]> dcAccounts = new Dictionary<string, string[]>();

        //Getter pro získání přihlášeného účtu
        public static string[] LogAccount { get; set; }
        
        //Getter pro uložení účtů
        public static Dictionary<string, string[]> GetAccounts { get { return dcAccounts; } }

        // Vytvoření účtu
        public static bool CreateNewAccount(string name, string password, string email)
        {
            try
            {
                //Přidej novou položku
                //Pokud toto jménu už existuje, vrátí se FALSE v Catch
                //Jinak se vrátí TRUE
                dcAccounts.Add(name, new string[] { password, email });
                return true;
            }
            catch (Exception) { return false; }
        }

        // Kontrola existence účtu
        public static bool CheckAccounts(string name)
        {
            //Existuje účet s tímto uživatelským jménem?
            if (dcAccounts.ContainsKey(name)) return true; //Účet existuje
            else return false; //Účet neexistuje
        }

        // Kontrola správného hesla
        public static bool CheckAccountPassword(string name, string password)
        {
            //Je heslo správně?
            if (dcAccounts[name][0] == password)
            {
                LogAccount = new string[] { name, dcAccounts[name][0], dcAccounts[name][1] };
                return true; //Správné heslo
            }
            else return false; //Špatné heslo
        }
    }
}
