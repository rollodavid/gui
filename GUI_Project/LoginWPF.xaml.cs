﻿using System;
using System.Windows;
using System.Windows.Input;

namespace GUI_Project
{
    /// <summary>
    /// Interakční logika pro LoginWPF.xaml
    /// </summary>
    public partial class LoginWPF : Window
    {
        #region Variables
        readonly int minDataLength = 1; //Minimální délka údajů (jméno, heslo)
        #endregion

        public LoginWPF()
        {
            InitializeComponent();
        }

        // Login
        private void Log_Click(object sender, RoutedEventArgs e)
        {
            //Údaje musí mít délku minimálně minDataLength
            if (logName.Text.Length >= minDataLength && logPass.Password.Length >= minDataLength)
            {
                //Existuje uživatelské jméno?
                if (Accounts.CheckAccounts(logName.Text))
                {
                    //Je heslo správně?
                    if (Accounts.CheckAccountPassword(logName.Text, logPass.Password))
                    {
                        MessageBox.Show($"Byl jste přihlášen, {logName.Text}.\nVítejte.", "Přihlášení", MessageBoxButton.OK, MessageBoxImage.Information);
                        Close();
                    }
                    else MessageBox.Show("Špatné heslo!", "Přihlášení", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
                else MessageBox.Show("Účet neexistuje!", "Přihlášení", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            else MessageBox.Show("Všechny údaje musí mít délku alespoň 1.", "Přihlášení", MessageBoxButton.OK, MessageBoxImage.Asterisk);
        }

        // Pohyb oknem
        private void Log_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed) DragMove();
        }

        // Zavírání okna
        private void Log_Close_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
