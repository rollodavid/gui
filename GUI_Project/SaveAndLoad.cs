﻿using System;
using System.Collections.Generic;
using System.IO;

namespace GUI_Project
{
    class SaveAndLoad
    {
        #region Variables
        static Dictionary<string, string[]> dc;
        static readonly string path = "save.txt";
        static string res;
        static string[] splitAccount;
        #endregion

        // Getter na získání cesty k zapnutí procesu (zobrazení souboru v MainWindow)
        public static string Path { get { return path; } }

        public static void SaveAccounts()
        {
            File.Create(path).Close();

            dc = Accounts.GetAccounts;
            res = "";

            foreach (string name in dc.Keys) res += $"{name};{dc[name][0]};{dc[name][1]}\n";

            File.WriteAllText(path, res);
        }

        public static void LoadAccounts()
        {
            if (File.Exists(path))
            {
                foreach (string account in File.ReadAllLines(path))
                {
                    splitAccount = account.Split(';');
                    try
                    {
                        Accounts.CreateNewAccount(splitAccount[0], splitAccount[1], splitAccount[2]);
                    }
                    catch (Exception)
                    {
                        System.Diagnostics.Debug.WriteLine("\n!!! Textový soubor s uloženými uživateli má špatný formát\n");
                    }
                }
            }
        }
    }
}
